package com.ihg.fpr;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class FprExport {
	
	public static void main(String args[]) throws ParserConfigurationException, SAXException, IOException{
		 DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		 DocumentBuilder db = dbf.newDocumentBuilder();
		 Document dom = db.parse("D:\\project\\ricoh\\11.xml");
		 Element documentElement = dom.getDocumentElement();
		 NodeList IssueNodeList = documentElement.getElementsByTagName("Issue");
		 
		 	HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Export");
			HSSFRow rowhead = sheet.createRow((short) 0);
			rowhead.createCell((short) 0).setCellValue("Vulnerability Name");
			rowhead.createCell((short) 1).setCellValue("Severity");
			rowhead.createCell((short) 2).setCellValue("File Path");
			rowhead.createCell((short) 3).setCellValue("Line Number");
			rowhead.createCell((short) 4).setCellValue("Snippet");
			System.out.println(IssueNodeList.getLength());
			int r=1;
		 if(IssueNodeList != null && IssueNodeList.getLength() > 0) {
			 
			 
			 System.out.println(IssueNodeList.getLength());
	            for(int i = 0 ; i < IssueNodeList.getLength();i++) {
	            	
	            	System.out.println(i);
	            	
	                Element issueElement = (Element)IssueNodeList.item(i);
	                String vulName = null;
	                String Severity = null;
	                String fileName = null;
	                String FilePath = null;
	                int LineNumber = 0;
	                String snippet = null;
	                String newsnippet = null;
	                
	                //Get Category Value
	                NodeList nl = issueElement.getElementsByTagName("Category");
	                if(nl != null && nl.getLength() > 0) {
	    	            Element el = (Element)nl.item(0);
	    	            vulName = el.getFirstChild().getNodeValue();
	    	        }
	             
	                //Get Severity Value
	                NodeList n2 = issueElement.getElementsByTagName("Friority");
	                if(n2 != null && n2.getLength() > 0) {
	    	            Element el = (Element)n2.item(0);
	    	            Severity = el.getFirstChild().getNodeValue();
	    	        }
	               
	                
	                
	              //  if(vulName.equals("Cross-Site Scripting: DOM")){
	                
	                
	                //Get Primary Value
	                NodeList primaryList = issueElement.getElementsByTagName("Primary");
	                for (int j = 0; j < primaryList.getLength(); j++) {
	    				Node nNode = primaryList.item(j);
	    				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	    					Element eElement = (Element) nNode;
	    					fileName =eElement.getElementsByTagName("FileName").item(0).getTextContent();
	    					FilePath =eElement.getElementsByTagName("FilePath").item(0).getTextContent();
	    					LineNumber = Integer.parseInt(eElement.getElementsByTagName("LineStart").item(0).getTextContent());
	    					snippet = eElement.getElementsByTagName("Snippet").item(0).getTextContent();
	    				}
	                }
	               
	                int snippetLength = snippet.length();
	                
	                if(snippetLength >= 32767){
	                	newsnippet = snippet.substring(0, 32767);
	                }else{
	                	newsnippet = snippet;
	                }
	                //System.out.println(newsnippet);
	                
	                HSSFRow row = sheet.createRow((short) r);
					//row.createCell((short) 0).setCellValue(fileName);
					row.createCell((short) 0).setCellValue(vulName);
					row.createCell((short) 1).setCellValue(Severity);
					row.createCell((short) 2).setCellValue(FilePath);
					row.createCell((short) 3).setCellValue(LineNumber);
					row.createCell((short) 4).setCellValue(newsnippet);
					sheet.autoSizeColumn(0);
					sheet.autoSizeColumn(1);
					sheet.autoSizeColumn(2);
					sheet.autoSizeColumn(3);
					sheet.autoSizeColumn(4);
					r++;
	                //}
	            }
	            String username = System.getProperty("user.name");
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
				LocalDate localDate = LocalDate.now();
				String Date = dtf.format(localDate).replace("/", "-");
				String outputFile = "D:\\project\\ricoh\\11.xls";
				FileOutputStream fileOut = new FileOutputStream(outputFile);
				workbook.write(fileOut);
				fileOut.close();
	            
	        }
	}

}
